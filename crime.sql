/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : crime

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-09-18 02:41:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_logs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_logs`;
CREATE TABLE `tbl_logs` (
  `cn` int(100) NOT NULL AUTO_INCREMENT,
  `userid` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `dateTimeCreate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cn`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_logs
-- ----------------------------
INSERT INTO `tbl_logs` VALUES ('50', '1', 'Mark Aguirre', '', '2017-09-07 10:06:39');
INSERT INTO `tbl_logs` VALUES ('51', '1', 'Mark Aguirre', '', '2017-09-07 10:10:37');
INSERT INTO `tbl_logs` VALUES ('52', '1', 'Mark Aguirre', '', '2017-09-07 10:44:11');
INSERT INTO `tbl_logs` VALUES ('53', '1', 'Mark Aguirre', '', '2017-09-07 10:50:21');
INSERT INTO `tbl_logs` VALUES ('54', '1', 'Mark Aguirre', '', '2017-09-07 10:51:04');
INSERT INTO `tbl_logs` VALUES ('55', '1', 'Mark Aguirre', '', '2017-09-07 10:56:24');
INSERT INTO `tbl_logs` VALUES ('56', '1', 'Mark Aguirre', '', '2017-09-07 10:58:51');
INSERT INTO `tbl_logs` VALUES ('57', '1', 'Mark Aguirre', '', '2017-09-15 21:40:42');
INSERT INTO `tbl_logs` VALUES ('58', '1', 'Mark Aguirre', '', '2017-09-15 21:41:33');
INSERT INTO `tbl_logs` VALUES ('59', '1', 'Mark Aguirre', '', '2017-09-15 21:42:37');
INSERT INTO `tbl_logs` VALUES ('60', '1', 'Mark Aguirre', '', '2017-09-15 21:45:47');
INSERT INTO `tbl_logs` VALUES ('61', '1', 'Mark Aguirre', '', '2017-09-15 22:02:28');
INSERT INTO `tbl_logs` VALUES ('62', '1', 'Mark Aguirre', '', '2017-09-15 22:06:01');
INSERT INTO `tbl_logs` VALUES ('63', '1', 'Mark Aguirre', '', '2017-09-15 22:14:53');
INSERT INTO `tbl_logs` VALUES ('64', '1', 'Mark Aguirre', '', '2017-09-15 22:16:16');
INSERT INTO `tbl_logs` VALUES ('65', '1', 'Mark Aguirre', '', '2017-09-15 22:18:33');
INSERT INTO `tbl_logs` VALUES ('66', '1', 'Mark Aguirre', '', '2017-09-15 22:36:08');
INSERT INTO `tbl_logs` VALUES ('67', '1', 'Mark Aguirre', '', '2017-09-16 00:46:28');

-- ----------------------------
-- Table structure for t_announcement
-- ----------------------------
DROP TABLE IF EXISTS `t_announcement`;
CREATE TABLE `t_announcement` (
  `cn` int(100) NOT NULL AUTO_INCREMENT,
  `Subject` varchar(30) DEFAULT NULL,
  `Description` varchar(254) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` varchar(255) DEFAULT NULL,
  `isNew` int(10) DEFAULT '1',
  PRIMARY KEY (`cn`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_announcement
-- ----------------------------
INSERT INTO `t_announcement` VALUES ('1', 'Pellentesque eget nunc. Donec ', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('2', 'Sed sagittis. Nam congue, risu', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('3', 'Phasellus id sapien in sapien ', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\r\n\r\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\r\n\r\nDonec diam neque, vestibu', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('4', 'Cras non velit nec nisi vulput', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\r\n\r\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermen', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('5', 'Nam congue, risus semper porta', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('6', 'In hac habitasse platea dictum', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet loborti', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('7', 'Lorem ipsum dolor sit amet, co', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('8', 'Etiam pretium iaculis justo. I', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\r\n\r\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('9', 'In hac habitasse platea dictum', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\r\n\r\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('10', 'Donec posuere metus vitae ipsu', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('11', 'Duis consequat dui nec nisi vo', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('12', 'Morbi vel lectus in quam fring', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.\r\n\r\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\r\n\r\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae n', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('13', 'Sed ante. Vivamus tortor.', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('14', 'Praesent id massa id nisl vene', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\r\n\r\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, ', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('15', 'Cras in purus eu magna vulputa', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.\r\n\r\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('16', 'Phasellus sit amet erat. Nulla', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('17', 'Vivamus tortor. Duis mattis eg', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('18', 'Nulla mollis molestie lorem. Q', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\r\n\r\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('19', 'Pellentesque eget nunc. Donec ', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\r\n\r\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\r\n\r\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam a', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('20', 'Praesent blandit. Nam nulla. I', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\r\n\r\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\r\n\r\nQuisque porta volutpat erat. Quisque erat ', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('21', 'Nulla facilisi. Cras non velit', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('22', 'Cum sociis natoque penatibus e', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('23', 'Integer aliquet, massa id lobo', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('24', 'Aliquam quis turpis eget elit ', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\r\n\r\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\r\n\r\nFusce consequat. Nu', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('25', 'Duis bibendum, felis sed inter', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nEtiam vel augue. Vestibulum rutrum rutrum n', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('26', 'Maecenas leo odio, condimentum', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('27', 'Donec odio justo, sollicitudin', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\r\n\r\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('28', 'Proin interdum mauris non ligu', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('29', 'Morbi a ipsum. Integer a nibh.', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\r\n\r\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultr', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('30', 'Morbi vestibulum, velit id pre', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\r\n\r\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('31', 'Nulla mollis molestie lorem. Q', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('32', 'Lorem ipsum dolor sit amet, co', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\r\n\r\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('33', 'Integer aliquet, massa id lobo', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\r\n\r\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('34', 'Quisque erat eros, viverra ege', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\r\n\r\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fau', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('35', 'Nulla facilisi. Cras non velit', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('36', 'Maecenas tristique, est et tem', 'Fusce consequat. Nulla nisl. Nunc nisl.\r\n\r\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('37', 'Quisque erat eros, viverra ege', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\r\n\r\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\r\n\r\nPhasellus', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('38', 'Aenean fermentum. Donec ut mau', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\r\n\r\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('39', 'Maecenas rhoncus aliquam lacus', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nEtiam vel augue. Vestibulum rutrum rutrum n', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('40', 'Vivamus vestibulum sagittis sa', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\r\n\r\nIn congue. Etiam justo. Etiam pretium iaculis justo.\r\n\r\nIn hac habitasse pl', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('41', 'Lorem ipsum dolor sit amet, co', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.\r\n\r\nMorbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\r\n\r\nFusce posuere felis sed l', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('42', 'Donec odio justo, sollicitudin', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\r\n\r\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('43', 'Integer aliquet, massa id lobo', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\r\n\r\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('44', 'Suspendisse accumsan tortor qu', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('45', 'Aliquam quis turpis eget elit ', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\r\n\r\nFusce consequat. Nulla nisl. Nunc nisl.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('46', 'Proin at turpis a pede posuere', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('47', 'Lorem ipsum dolor sit amet, co', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\r\n\r\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('48', 'Morbi sem mauris, laoreet ut, ', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet loborti', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('49', 'Curabitur at ipsum ac tellus s', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\r\n\r\nIn congue. Etiam justo. Etiam pretium iaculis justo.\r\n\r\nIn hac habitasse pl', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('50', 'In blandit ultrices enim. Lore', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\r\n\r\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('51', 'Maecenas pulvinar lobortis est', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\r\n\r\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicu', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('52', 'Phasellus sit amet erat. Nulla', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\r\n\r\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ant', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('53', 'Curabitur gravida nisi at nibh', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('54', 'Vivamus tortor.', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis p', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('55', 'Phasellus id sapien in sapien ', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\r\n\r\nNullam p', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('56', 'Pellentesque eget nunc.', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\r\n\r\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\r\n\r\nDonec diam neque, vestibu', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('57', 'Quisque porta volutpat erat. Q', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\r\n\r\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\r\n\r\nMorbi porttitor lorem id ligula. Suspen', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('58', 'Duis at velit eu est congue el', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\r\n\r\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\r\n\r\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis mo', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('59', 'Maecenas pulvinar lobortis est', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('60', 'Aliquam non mauris. Morbi non ', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\r\n\r\nFusce consequat. Nulla nisl. Nunc nisl.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('61', 'Morbi porttitor lorem id ligul', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('62', 'Integer a nibh. In quis justo.', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\r\n\r\nNullam sit amet turpis elementum ligul', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('63', 'Morbi sem mauris, laoreet ut, ', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('64', 'Nulla tellus. In sagittis dui ', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\r\n\r\nNullam p', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('65', 'Donec dapibus. Duis at velit e', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('66', 'Vivamus vestibulum sagittis sa', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nEtiam vel augue. Vestibulum rutrum rutrum n', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('67', 'Maecenas pulvinar lobortis est', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\r\n\r\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('68', 'In blandit ultrices enim.', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\r\n\r\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\r\n\r\nNam ultrices, libero non mattis pulvinar, nulla pede u', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('69', 'Cum sociis natoque penatibus e', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\r\n\r\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\r\n\r\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis mo', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('70', 'Fusce congue, diam id ornare i', 'Fusce consequat. Nulla nisl. Nunc nisl.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('71', 'Aliquam augue quam, sollicitud', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.\r\n\r\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('72', 'Duis bibendum. Morbi non quam ', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\r\n\r\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Null', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('73', 'Donec ut dolor. Morbi vel lect', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\r\n\r\nPhasellus in felis. Donec semper sapien a libero. Nam dui.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('74', 'Pellentesque eget nunc. Donec ', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\r\n\r\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('75', 'In hac habitasse platea dictum', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\r\n\r\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('76', 'Aliquam augue quam, sollicitud', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\r\n\r\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui ', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('77', 'Curabitur convallis. Duis cons', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\r\n\r\nPhasellus in felis. Donec semper sapien a libero. Nam dui.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('78', 'Donec ut mauris eget massa tem', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('79', 'Donec odio justo, sollicitudin', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\r\n\r\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('80', 'Mauris enim leo, rhoncus sed, ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\r\n\r\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis fau', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('81', 'Donec odio justo, sollicitudin', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nEtiam vel augue. Vestibulum rutrum rutrum n', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('82', 'Vestibulum sed magna at nunc c', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\r\n\r\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('83', 'Donec dapibus. Duis at velit e', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\r\n\r\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('84', 'Fusce congue, diam id ornare i', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('85', 'Integer ac leo.', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.\r\n\r\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('86', 'Etiam vel augue. Vestibulum ru', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('87', 'Integer ac leo. Pellentesque u', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\r\n\r\nVestibulum ac ', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('88', 'Vestibulum ante ipsum primis i', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nEtiam vel augue. Vestibulum rutrum rutrum n', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('89', 'Aliquam sit amet diam in magna', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\r\n\r\nFusce consequat. Nulla nisl. Nunc nisl.\r\n\r\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in port', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('90', 'Donec ut dolor. Morbi vel lect', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('91', 'Donec ut mauris eget massa tem', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\r\n\r\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus ', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('92', 'In hac habitasse platea dictum', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('93', 'Aliquam augue quam, sollicitud', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\r\n\r\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\r\n\r\nMorbi porttitor lorem id ligula. Suspen', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('94', 'Nulla nisl. Nunc nisl. Duis bi', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\r\n\r\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('95', 'Donec odio justo, sollicitudin', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('96', 'Vestibulum ante ipsum primis i', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\r\n\r\nFusce consequat. Nulla nisl. Nunc nisl.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('97', 'Mauris enim leo, rhoncus sed, ', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\r\n\r\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placer', '2017-06-02 14:39:33', '1', '0');
INSERT INTO `t_announcement` VALUES ('98', 'Suspendisse ornare consequat l', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\r\n\r\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('99', 'Duis consequat dui nec nisi vo', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2017-06-02 14:39:33', '0', '0');
INSERT INTO `t_announcement` VALUES ('100', 'Phasellus in felis. Donec semp', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2017-06-02 14:39:33', '0', '0');

-- ----------------------------
-- Table structure for t_files
-- ----------------------------
DROP TABLE IF EXISTS `t_files`;
CREATE TABLE `t_files` (
  `cn` int(100) NOT NULL AUTO_INCREMENT,
  `FileCode` varchar(100) DEFAULT NULL,
  `Filename` varchar(100) DEFAULT NULL,
  `FileType` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cn`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_files
-- ----------------------------
INSERT INTO `t_files` VALUES ('101', '1013', '1013pic', '.png');

-- ----------------------------
-- Table structure for t_report
-- ----------------------------
DROP TABLE IF EXISTS `t_report`;
CREATE TABLE `t_report` (
  `cn` int(100) NOT NULL AUTO_INCREMENT,
  `Subject` varchar(20) DEFAULT NULL,
  `Description` longtext,
  `Lat` varchar(254) DEFAULT NULL,
  `Lng` varchar(254) DEFAULT NULL,
  `Media_code` varchar(100) DEFAULT NULL,
  `Sender_fbid` varchar(100) DEFAULT NULL,
  `ContactNo` varchar(100) DEFAULT NULL,
  `DateCreated` varchar(100) DEFAULT '',
  `Status` varchar(100) DEFAULT 'Pendeng',
  `Remark` int(100) DEFAULT '0',
  `isNew` int(100) DEFAULT '1',
  PRIMARY KEY (`cn`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_report
-- ----------------------------
INSERT INTO `t_report` VALUES ('1', 'Medical', 'sanple message', '14.537309', '120.9915819', '1013', '1', '09567885945', '16/8/2017', 'Done', '1', '0');
INSERT INTO `t_report` VALUES ('2', 'Medical', 'sanple message', '14.533093', '120.989211', '1013', '1', '09567885945', '16/8/2017', 'Pending', '1', '0');
INSERT INTO `t_report` VALUES ('3', 'Medical', 'gdhd', '14.5465076', '121.0033374', '702', '1', '09567885945', '15/9/2017', 'Done', '0', '0');
INSERT INTO `t_report` VALUES ('4', 'Medical', 'sampsns', '14.5464926', '121.003371', '2198', '1', '09567885945', '16/9/2017', 'Pendeng', '0', '1');

-- ----------------------------
-- Table structure for t_subject
-- ----------------------------
DROP TABLE IF EXISTS `t_subject`;
CREATE TABLE `t_subject` (
  `cn` int(100) NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) DEFAULT NULL,
  `Reamark` int(10) DEFAULT NULL,
  PRIMARY KEY (`cn`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_subject
-- ----------------------------
INSERT INTO `t_subject` VALUES ('1', 'Fire', '0');
INSERT INTO `t_subject` VALUES ('2', 'Accident', '0');
INSERT INTO `t_subject` VALUES ('3', 'Arson', '0');
INSERT INTO `t_subject` VALUES ('4', 'Bribery', '0');
INSERT INTO `t_subject` VALUES ('5', 'Carnapping', '0');
INSERT INTO `t_subject` VALUES ('6', 'Child Abuse', '0');
INSERT INTO `t_subject` VALUES ('7', 'Domestic Violence', '0');
INSERT INTO `t_subject` VALUES ('8', 'Extortion', '0');
INSERT INTO `t_subject` VALUES ('9', 'Hold-up', '0');
INSERT INTO `t_subject` VALUES ('10', 'Homicide', '0');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `cn` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `usertype` int(100) DEFAULT '2',
  `dateregister` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateby` int(10) DEFAULT NULL,
  `isdelete` int(10) DEFAULT '0',
  `isNew` int(10) DEFAULT '1',
  `isApproved` int(10) DEFAULT '0',
  PRIMARY KEY (`cn`),
  KEY `cn` (`cn`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'markaguirre.r@gmail.com', '5d793fc5b00a2348c3fb9ab59e5ca98a', 'Mark Aguirre', 'Male', '38', 'address', '1', '0000-00-00 00:00:00', '1', '0', '0', '0');
INSERT INTO `t_user` VALUES ('2', 'qqqqq@g.com', '5d793fc5b00a2348c3fb9ab59e5ca98a', 'kkk', 'Male', '16', 'bebehs', '2', '2017-09-15 21:39:04', '1', '1', '0', '1');
INSERT INTO `t_user` VALUES ('14', '.r@h.j.', 'ea7fd8f4a6a5823e90149644def7a306', '.f.', '.Female.', '2', '.dd.', '2', '2017-09-15 23:33:10', null, '0', '0', '1');
INSERT INTO `t_user` VALUES ('15', 'aaaaaa@d.n', 'c6783dea8f4f01381dad8bb844a72eff', '.vvhh.', '.Female.', '1', '.dff.', '2', '2017-09-15 23:53:31', null, '0', '0', '1');

-- ----------------------------
-- Procedure structure for sp_changePassword
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_changePassword`;
DELIMITER ;;
CREATE  PROCEDURE `sp_changePassword`(IN `ID1` varchar(100),IN `ID2` varchar(100))
BEGIN
	#Routine body goes here...
UPDATE `t_user` SET `password`=MD5(ID1) WHERE (`cn`=ID2);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_login
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_login`;
DELIMITER ;;
CREATE  PROCEDURE `sp_login`(IN `ID1` varchar(100),IN `ID2` varchar(100))
BEGIN
	#Routine body goes here...
SELECT *  FROM t_user WHERE BINARY username = ID1  AND BINARY `password` = md5(REPLACE(ID2,"'","+")) AND isApproved = 1;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_logs
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_logs`;
DELIMITER ;;
CREATE  PROCEDURE `sp_logs`()
BEGIN
	#Routine body goes here...
SELECT
tbl_logs.cn,
tbl_logs.userid,
tbl_logs.fullname,
tbl_logs.email,
DATE_FORMAT(tbl_logs.dateTimeCreate, "%r %W %M %e %Y") as DateTime

FROM
tbl_logs
ORDER BY
tbl_logs.cn DESC;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_new_data
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_new_data`;
DELIMITER ;;
CREATE  PROCEDURE `sp_new_data`()
BEGIN
	#Routine body goes here...
SELECT  (SELECT COUNT(cn) FROM   t_report WHERE `isNew` = '1') AS reportnew,
				(SELECT COUNT(cn) FROM   t_user WHERE   `isNew` = '1') AS usernew,
				(SELECT COUNT(cn) FROM   t_announcement WHERE `isNew` = '1') AS anmtnew;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_register
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_register`;
DELIMITER ;;
CREATE  PROCEDURE `sp_register`(IN `ID1` varchar(100),IN `ID2` varchar(100),IN `ID3` varchar(100),IN `ID4` varchar(100),IN `ID5` varchar(100),IN `ID6` varchar(100),IN `ID7` varchar(100),IN `ID8` blob)
BEGIN
	#Routine body goes here...
INSERT INTO `t_user` (`username`, `password`, `fullname`, `gender`,`usertype`,`age`,`address`,`Media`) VALUES (ID1, md5(REPLACE(ID2,"'","+")), ID3, ID4, ID5, ID6, ID7,ID8);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_report`;
DELIMITER ;;
CREATE  PROCEDURE `sp_report`()
BEGIN
	#Routine body goes here...
SELECT
t_report.cn,
t_report.`Subject`,
t_report.ContactNo,
t_report.DateCreated,
t_report.`Status`,
t_report.isNew
FROM t_report
ORDER BY t_report.cn DESC;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_report_bycn
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_report_bycn`;
DELIMITER ;;
CREATE  PROCEDURE `sp_report_bycn`(IN `ID` int(100))
BEGIN
	#Routine body goes here...
SELECT
t.cn,
t.`Subject`,
t.Description,
t.Lat,
t.Lng,
t.Media_code,
t.Sender_fbid,
t.ContactNo,
t.DateCreated,
t.`Status` as stat,
a.fullname
FROM
t_report AS t
LEFT JOIN tbl_logs as a ON t.Sender_fbid = a.userid
WHERE
t.cn = ID
ORDER BY
t.cn DESC
LIMIT 1;




END
;;
DELIMITER ;
