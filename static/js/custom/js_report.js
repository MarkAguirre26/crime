$(document).ready(function(){

    $(function () {
        $.ajax({
            type: "GET",
            url: "fetchReport",
            dataType: "json",
            success: function(data){
                var table =   $('#records_table').DataTable({
                    "bProcessing": true,
                    "deferRender": true,
                    data: data,
                    columns:[
                        {title:"#"},
                        {title: "Subject"},
                        {title:"ContactNo"},
                        {title:"Date"},
                        {title:"Status"}  ,
                        {title:"isNew"}  ,
                        {title:""}
                    ],"columnDefs": [ {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<span><a href='#'  id='a_info' style='margin-left: 10px' class='glyphicon glyphicon-info-sign'></a></span>",

                    } ],
                     "order": [[ 3, "desc" ]]

                });

                $('#records_table tbody').on('click', '#a_info', function () {
                    var d = table.row( $(this).parents('tr') ).data();
                    localStorage.setItem("report_cn", d[0]);
                    window.location.href = flask_util.url_for('report_info', {});

                        $.ajax({
                            url: '/reportisNew',
                            data: {"cn":d[0]},
                            type: 'POST',
                            success: function(response) {
                                console.log(response);
                            },
                            error: function(error) {
                                console.log(error);

                            }
                        });

                } );
            },
            error:function (xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }

        });
    });
});