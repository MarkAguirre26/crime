function initialize() {
    var locations;
    $.ajax({
        url:"/fetchMap",
        dataType: "json",
        success:function (data) {
            locations = data;

            var map = new google.maps.Map(document.getElementById('googleMap'), {
                zoom: 11,
                center: new google.maps.LatLng(14.563809, 121.020644),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;
            var base_url = location.protocol + '//' + location.host + '/';

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon:base_url+"static/images/red_marker.png"

                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }



        }
    });

    $.ajax({
        url:"/getNewCount",
        type:"GET",
        success:function (e) {
            $("#report_count_notif").html(e.records.reportnew);
            $("#user_count_notif").html(e.records.usernew);
            // $("#announcement_count_notif").html(e.records.anmtnew);
            // $("#msg_count_notif").html("0");


        }
    });


    $("#aprofile").click(function () {
        $.ajax({
            type:"GET",
            url:"/user/getbyid/"+ $("#ntxt_cn").val(),
            success: function(data){
                console.log(data);
                // $("#modal_title").text("Profile");

                // $("input[name=txt_cn]").val(data.records.cn);
                $("#ntxt_action").val("Update");
                $("#ntxt_name").val(data.records.name);
                $("#ntxt_email").val(data.records.email);
                $("#ncbo_gender").val(data.records.gender);
                $("#ncbo_type").val(data.records.usertype);
                $("#ntxt_age").val(data.records.age);
                $("#ntxt_address").val(data.records.address);
                $("#ntxt_since").val(data.records.dateregister);
                $('#cmd_profileSave').text("Save Changes")
                $('#ProfileModals').modal('show');

            }

        });
    });



    $('#cmd_profileSave').click(function() {
                 $.ajax({
                url: '/ModifyUsaer',
                data: {'cn': $("#ntxt_cn").val(), 'action': $("#ntxt_action").val(), 'name': $("#ntxt_name").val(), 'email': $("#ntxt_email").val(), 'password':$("#ntxt_password").val(), 'repeatpassword': $("#ntxt_repeatpassword").val(), 'gender': $("#ncbo_gender").val(), 'usertype': $("#ncbo_type").val(), 'age': $("#ntxt_age").val(), 'address': $("#ntxt_address").val()},
                type: 'POST',
                success: function(response) {
                    console.log(response);
                    var r = response.status.status;
                    try {
                        if(r == "False"){
                            $("#nuser_error").show();

                        }else{
                            $('#ProfileModals').modal('hide');
                            // location.reload();
                        }
                    }
                    catch(err) {
                        $("#nuser_error").show();

                    }
                }

            });

    });



    $('#nuserx').click(function() {
        $("#nuser_error").hide();

    });



}


