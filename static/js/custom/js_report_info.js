
// function initialize() {
var lat,lng,mapProp,map,marker;

$.ajax({
    type: "GET",
    url: "/reportbyid/" + localStorage.getItem('report_cn'),
    success: function (response) {
        $("#txt_cn").val(response.data.report_cn);
        $("#txt_subject").val(response.data.Subject);
        $("#txt_description").val(response.data.Description);
        $("#txt_sender").val(response.data.fullname);
        $("#txt_contact").val(response.data.ContactNo);

        $('#gotoGoogleMap').attr('href',"https://www.google.com/maps/search/?api=1&query="+response.data.Lat+","+response.data.Lng+"")
        // $('#gotoGoogleMap').attr('');
        // $("#txt_filecode").val(response.data.fileCode);
        $("select[name=cbo_status]").val(response.data.Status);

        localStorage.setItem('fcode',response.data.fileCode);
        localStorage.setItem('report_cn',response.data.report_cn);

        // lat = response.data.Lat;
        // lng =  response.data.Lng;

        // localStorage.setItem('lat',lat);
        // localStorage.setItem('lng',lng);
        // var myLatlng = new google.maps.LatLng(lat,lng);
        //  var base_url = location.protocol + '//' + location.host + '/';
        // mapProp= {
        //     center:myLatlng,
        //     zoom:17,
        // };
        // marker=new google.maps.Marker({
        //     position:myLatlng,
        //      icon:base_url+"static/images/red_marker.png",
        //
        // });
        //
        // map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
        // marker.setMap(map);
    }


});


$(function() {

    $('#cbo_status').change(function() {
        var c =  localStorage.getItem("report_cn");
        var s =  $("select[name=cbo_status]").val();
        $.ajax({
            url: '/modifustatus',
            data: {"rcn":c,"status":s},//$('form').serialize()
            type: 'POST',
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log(error);

            }
        });
    });
});

$(function () {
    $("#imgtabs").click(function () {

        $.ajax({
            url: "/getImages/"+localStorage.getItem('fcode'),
            success:function (response) {

                var obj  = JSON.parse(response)
                var base_url = location.protocol + '//' + location.host + '/static/images/photos/';
                // $.each(obj,function(index,value){
                //     $("#imglist").append('<ul>'+(index+1)+'</ul>');
                //     return (index !== 9);
                // });
                for(i = 0; i < obj.length;i++){
                    var path  = base_url+obj[i]+".png";
                    console.log(path);
                    $("#imglist").append('<div class="column">' +
                        '<img src="'+path+'"  onclick="openModal();currentSlide(1)" class="hover-shadow cursor img-thumbnail">' +
                        ' </div>');
                    $("#myslide-item").html('<img src="'+path+'" class="img-center">');


                    $("#modal-thumb").append('<div class="column">' +
                        ' <img class="demo cursor img-thumbnail" src="'+path+'"  onclick="currentSlide(1)"" >' +
                        ' </div>');

                }

            }
        });
    });

});


function openModal() {
    document.getElementById('myModal').style.display = "block";
}

function closeModal() {
    document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    // dots[slideIndex-1].className += " active";
    // captionText.innerHTML = dots[slideIndex-1].alt;
}

