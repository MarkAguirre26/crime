$(document).ready(function () {
    $("#cmd_new").click(function () {
        $("#modal_title").text("Create Account");
        // $("#div_pwd").css("display","none");
        // $("#div_since").hide();
        $('#divpwd').removeClass("hidediv");
        $("#txt_label_since").hide();

        $("#cmd_save_user").text("Save");
        $("input[name=txt_action]").val("New");
        $("input[name=txt_name]").val('');
        $("input[name=txt_email]").val('');
        $("input[name=txt_password]").val('');
        $("select[name=cbo_gender]").val('');
        $("select[name=cbo_type]").val('');
        $("input[name=txt_age]").val('');
        $("textarea[name=txt_address]").val('');
        $("input[name=txt_since]").hide();
        $('#userModal').modal('show');


    });

    $(function () {
        $('#userx').click(function () {
            $("#user_error").hide();
        });
    });

    $("input[name=txt_password]").change(function () {
        var pwd = $("#txt_password").val();
        var s = checkPwd(pwd);
        $("#p-error-pwd").text(s);


        if (s.length != 2) {

            $("#p-error-pwd").text(s);
        } else {

            $("#p-error-pwd").text("");
        }

    });

    $(function () {
        $("input[name=txt_repeatpassword]").change(function () {
            var pwd = $("#txt_password").val();
            var repwd = $("#txt_repeatpassword").val();
            if (pwd != repwd) {
                $("#p-error-repwd").text("Password not match");
            } else {
                $("#p-error-repwd").text("");

            }
        });
    });
    $(function () {
        $('#cmd_save_user').click(function () {


            $.ajax({
                url: '/ModifyUsaer',
                data: {
                    'cn': $("#txt_cn").val(),
                    'action': $("#txt_action").val(),
                    'name': $("#txt_name").val(),
                    'email': $("#txt_email").val(),
                    'password': $("#txt_password").val(),
                    'repeatpassword': $("#txt_repeatpassword").val(),
                    'gender': $("#cbo_gender").val(),
                    'usertype': $("#cbo_type").val(),
                    'age': $("#txt_age").val(),
                    'address': $("#txt_address").val()
                },
                type: 'POST',
                success: function (response) {
                    console.log(response);
                    var r = response.status.status;
                    try {
                        if (r == "False") {
                            // alert(r);
                            $("#user_error").show();

                        } else {
                            $('#userModal').modal('hide');
                            window.location.href = flask_util.url_for('user', {});
                        }
                    }
                    catch (err) {
                        $("#user_error").show();

                    }
                }

            });


        });
    });

    function checkPwd(str) {
        if (str.length < 8) {
            return ("Too short");
        } else if (str.length > 50) {
            return ("Too long");
        } else if (str.search(/\d/) == -1) {
            return ("No number");
        } else if (str.search(/[a-z]/) == -1) {
            return ("No small letter");
        } else if (str.search(/[A-Z]/) == -1) {
            return ("No Caps letter");
        } else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {
            return ("Bad char");
        }
        return ("ok");
    }

    $(function () {
        $.ajax({
            type: "GET",
            url: "/user/fetch",
            dataType: "json",
            success: function (data) {


                var table = $('#records_table').DataTable({
                    "bProcessing": true,
                    "deferRender": true,
                    data: data,
                    columns: [
                        {title: "#"},
                        {title: "username"},
                        {title: "Name"},
                        {title: "Gender"},
                        {title: "Age"},
                        {title: "isNew"},
                        {title: "isApproved"},
                        {title: ""}
                    ], "columnDefs": [{
                        "width": "90px",
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<span><a style='margin-left: 10px' href='#' id='a_approved_user' class='glyphicon glyphicon-check'></a></span><span><a style='margin-left: 10px' href='#' id='a_delete_user' class='glyphicon glyphicon-remove'></a></span><span><a style='margin-left: 10px' href='#'  id='a_info_user' style='margin-left: 10px' class='glyphicon glyphicon-info-sign'></a></span>",

                    }],
                    "order": [[3, "desc"]]

                });

                $('#records_table tbody').on('click', '#a_approved_user', function () {
                    var d = table.row($(this).parents('tr')).data();
                    if (confirm("Are you sure you want to approve " + d[2] + "?")) {
                        $.ajax({
                            url: '/user/approved',
                            data: {"cn": d[0].toString()},
                            type: 'POST',
                            success: function (response) {
                                //console.log(response);

                                $.ajax({
                                    url: '/sendmMail',
                                    data: {"email": d[1].toString(), "messageFrom": "You account has been activated"},
                                    type: 'POST',
                                    success: function (response) {
                                        console.log(response)
                                        location.reload();
                                    },
                                    error: function (error) {
                                        console.log(error);
                                    }
                                });


                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }

                });


                $('#records_table tbody').on('click', '#a_delete_user', function () {
                    var d = table.row($(this).parents('tr')).data();
                    if (confirm("Are you sure you want to remove " + d[2] + "?")) {
                        table.row($(this).parents('tr')).remove().draw();
                        $.ajax({
                            url: '/user/deleteuser',
                            data: {"cn": d[0].toString()},
                            type: 'POST',
                            success: function (response) {
                                console.log(response);
                                 $.ajax({
                                    url: '/sendmMail',
                                    data: {"email": d[1].toString(), "messageFrom": "You account has been deactivated"},
                                    type: 'POST',
                                    success: function (response) {
                                        console.log(response)
                                       // location.reload();
                                    },
                                    error: function (error) {
                                        console.log(error);
                                    }
                                });

                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }

                });
                $('#records_table tbody').on('click', '#a_info_user', function () {


                    var d = table.row($(this).parents('tr')).data();

                    $.ajax({
                        type: "GET",
                        url: "/user/getbyid/" + d[0],
                        success: function (data) {


                            var base_url = location.protocol + '//' + location.host + '/static/images/photos/';
                            var path = base_url + data.records.username + ".png";
                            console.log(path);

                            $("#userphoto").html('<img src="' + path + '" width="150px"  >');


                            $("#modal_title").text("User Details");
                            $('#divpwd').addClass("hidediv");
                            $("#txt_label_since").show();
                            $("input[name=txt_cn]").val(data.records.cn);
                            $("input[name=txt_action]").val("Update");
                            $("input[name=txt_name]").val(data.records.fullname);
                            $("input[name=txt_email]").val(data.records.username);
                            $("select[name=cbo_gender]").val(data.records.gender);
                            $("select[name=cbo_type]").val(data.records.usertype);
                            $("input[name=txt_age]").val(data.records.age);
                            $("textarea[name=txt_address]").val(data.records.address);
                            $("input[name=txt_since]").val(data.records.dateregister);
                            $("input[name=txt_since]").show();
                            $('#cmd_save_user').text("Update")
                            $('#userModal').modal('show');

                        }

                    });

                    $.ajax({
                        url: '/userisNew',
                        data: {"cn": d[0].toString()},
                        type: 'POST',
                        success: function (response) {
                            //console.log(response);
                        },
                        error: function (error) {
                            console.log(error);

                        }
                    });
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }

        });
    });
});