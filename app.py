import random
from datetime import datetime
from functools import wraps, update_wrapper
import pygal

from flask import Flask, request, render_template, session, abort, flash, jsonify, redirect, \
    url_for, make_response

from dbconnect import connection
from flask_util_js import FlaskUtilJs

import json
import os
import smtplib
from flask_mail import Mail, Message

app = Flask(__name__)
fujs = FlaskUtilJs(app)

mail = Mail(app)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'youremailhere'
app.config['MAIL_PASSWORD'] = 'yourpasswordhere'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.now()
        response.headers[
            'Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return update_wrapper(no_cache, view)


@app.route('/')
def index():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return render_template('index.html')


@app.route('/sendmMail', methods=["POST"])
def sendmMail():
    try:
        #random_number = random.randint(1, 1000)
        recipient = request.form['email']
        messageFrom = request.form['messageFrom']
        msg = Message('Hello', sender='markaguirre.r@gmail.com', recipients=[recipient])
        msg.body = messageFrom
        mail.send(msg)
        data = {'msg': "True", "messageFrom": messageFrom, "Email": recipient}
        return jsonify({'status': data})

    except Exception as e:
        return str(e)


@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/checklogin')
def checklogin():
    username = request.args.get('username')
    password = request.args.get('password')
    c, conn = connection()
    c.execute("call sp_login('%s','%s')" % (str(username), str(password)))
    user = c.fetchall()
    if len(user) is 1:
        for u in user:
            session['user_cn'] = u[0]
            session['fullName'] = u[3]
            session['user_type'] = u[7]
            session['logged_in'] = True
            return jsonify(result='true')
    else:
        return jsonify(result='false')


@app.route('/report')
def report():
    return render_template('report.html')


@app.route('/announcement')
def announcement():
    return render_template('announcement.html')


@app.route('/announcement/fetch')
def fetch_announcement():
    try:
        c, conn = connection()
        c.execute(
            "SELECT cn, `Subject`, DateCreated, isDeleted FROM t_announcement WHERE isDeleted = 0 ORDER BY cn DESC")
        data_list = c.fetchall()
        return json.dumps(data_list, default=str, sort_keys=True, ensure_ascii=False)
    except Exception as e:
        return str(e)


@app.route('/user')
def user():
    return render_template('user.html')


# get all users
@app.route("/user/fetch")
def fetch_users():
    try:
        c, conn = connection()
        c.execute(
            "SELECT cn,username,fullname, gender, age, isNew,isApproved FROM t_user  ORDER BY cn,isNew DESC")
        users = c.fetchall()
        return json.dumps(users, sort_keys=True, ensure_ascii=False)
    except Exception as e:
        return str(e)


@app.route("/logs/fetch")
def fetch_logs():
    try:
        c, conn = connection()
        c.execute("call sp_logs()")
        logs = c.fetchall()
        return json.dumps(logs, sort_keys=True, ensure_ascii=False)
    except Exception as e:
        return str(e)


# get user by id
@app.route("/user/getbyid/<int:cn>")
def get_user(cn):
    try:
        c, conn = connection()
        c.execute("SELECT * FROM t_user WHERE cn=%s" % (str(cn)))
        user = c.fetchone()
        data = {
            'cn': user[0],
            'fullname': user[3],
            'username': user[1],
            'gender': user[4],
            'age': user[5],
            'address': user[6],
            'usertype': user[7],
            'dateregister': user[8],
            'password': user[2]
        }
    except KeyError:
        abort(404)
    return jsonify({'records': data})


# Delete User
@app.route("/user/deleteuser", methods=["POST"])
def deleteuser():
    try:
        cn = request.form['cn']
        Approved = '0'
        isNew = '0'
        c, conn = connection()
        c.execute("UPDATE `t_user` SET isApproved = %s,isNew= %s WHERE (`cn`=%s)",
                  (Approved, isNew, str(cn)))
        conn.commit()
        data = {'msg': "True", 'cn': cn}
        return jsonify({'status': data})
    except Exception as e:
        return str(e)


# Approved User
@app.route("/user/approved", methods=["POST"])
def approveduser():
    try:
        cn = request.form['cn']
        Approved = '1'
        isNew = '0'
        c, conn = connection()
        c.execute("UPDATE `t_user` SET isApproved = %s,isNew= %s WHERE (`cn`=%s)",
                  (Approved, isNew, str(cn)))
        conn.commit()
        data = {'msg': "True", 'cn': cn}
        return jsonify({'status': data})
    except Exception as e:
        return str(e)


# update report status
@app.route('/modifustatus', methods=['POST'])
def modifustatus():
    try:
        status = request.form['status']
        cn = request.form['rcn']
        c, conn = connection()
        c.execute("UPDATE `t_report` SET `Status`=%s WHERE (`cn`=%s)", (status, str(cn)))
        conn.commit()
        data = {'msg': "True", 'status': status, 'cn': cn}
        return jsonify({'status': data})
    except Exception as e:
        return str(e)


@app.route('/logs')
def logs():
    return render_template('logs.html')


# update report isnew
@app.route('/reportisNew', methods=['POST'])
def reportisNew():
    try:
        cn = request.form['cn']
        val = '0'
        c, conn = connection()
        c.execute("UPDATE `t_report` SET `isNew`=%s WHERE (`cn`=%s)", (val, str(cn)))
        conn.commit()
        data = {'msg': "True", 'cn': cn}
        return jsonify({'status': data})
    except Exception as e:
        return str(e)


# update user isnew
@app.route('/userisNew', methods=['POST'])
def userisNew():
    try:
        cn = request.form['cn']
        val = '0'
        c, conn = connection()
        c.execute("UPDATE `t_user` SET `isNew`=%s WHERE (`cn`=%s)", (val, str(cn)))
        conn.commit()
        data = {'msg': "True", 'cn': cn}
        return jsonify({'status': data})
    except Exception as e:
        return str(e)


# update anmt isnew
@app.route('/anmtisNew', methods=['POST'])
def anmtisNew():
    try:
        cn = request.form['cn']
        val = '0'
        c, conn = connection()
        c.execute("UPDATE `t_announcement` SET `isNew`=%s WHERE (`cn`=%s)", (val, str(cn)))
        conn.commit()
        data = {'msg': "True", 'cn': cn}
        return jsonify({'status': data})
    except Exception as e:
        return str(e)


# Add User
@app.route('/ModifyUsaer', methods=['POST'])
def ModifyUsaer():
    try:
        cn = request.form['cn']
        action = request.form['action']
        name = request.form['name']
        email = request.form['email']
        gender = request.form['gender']
        usertype = request.form['usertype']
        age = request.form['age']
        address = request.form['address']
        pwd = request.form['password']
        repassword = request.form['repeatpassword']
        if action == "New":
            if name == "" or email == "" or gender == "" or age == "" or usertype == "" or address == "" or len(
                    pwd) < 6 or repassword != pwd:
                data = {'status': "False", 'action': action, 'name': name, 'email': email,
                        'gneder': gender, 'usertype': usertype, 'age': age, 'address': address}
                return jsonify({'status': data})
            else:
                c, conn = connection()
                c.execute(
                    "INSERT INTO `t_user` (`username`,`password`, `fullname`, `gender`,`age`, `address`,`usertype`) VALUES (%s,%s, %s, %s, %s,%s,%s)",
                    (email, pwd, name, gender, str(age), address, usertype))
                conn.commit()
                data = {'msg': "True"}
                return jsonify({'status': data})
        else:
            if name == "" or email == "" or gender == "" or age == "" or \
                            address == "" or usertype == "":
                data = {'status': "False", 'action': action, 'name': name,
                        'email': email,
                        'gender': gender, 'usertype': usertype, 'age': age, 'address': address}
                return jsonify({'status': data})
            else:
                c, conn = connection()
                c.execute(
                    "UPDATE `t_user` SET `username`=%s, `fullname`=%s, `gender`=%s, `age`=%s, `address`=%s,`usertype`=%s, `updateby`=%s WHERE (`cn`=%s)",
                    (email, name, gender, str(age), address, usertype, session.get('user_cn'), cn))
                conn.commit()
                data = {'msg': "True"}
                return jsonify({'status': data})

    except Exception as e:
        return str(e)


@app.route('/statistics')
def statistics():
    graph = pygal.Bar()
    graph.title = 'Reported Crime Incident Statistics'
    graph.x_labels = map(str, range(1, 12))
    graph.add('Firefox', [10, None, 0, 16.6, 25, 31, 36.4, 45.5, 46.3, 42.8, 37.1])
    graph.add('Chrome', [None, None, None, None, None, None, 0, 3.9, 10.8, 23.8, 35.3])
    graph.add('IE', [85.8, 84.6, 84.7, 74.5, 66, 58.6, 54.7, 44.8, 36.2, 26.6, 20.1])
    graph.add('Others', [14.2, 15.4, 15.3, 8.9, 9, 10.4, 8.9, 5.8, 6.7, 6.8, 7.5])
    graph_data = graph.render_data_uri()
    return render_template('statistics.html', graph_data=graph_data)


@app.route('/labout')
def about():
    return render_template('about.html')


@app.route('/signUpUser', methods=['POST'])
def signUpUser():
    user = request.form['username']
    password = request.form['password']
    return json.dumps({'status': 'OK', 'user': user, 'pass': password})


@app.route('/recover')
def recover():
    return render_template('forgotpassword.html')


# logout
@app.route('/logout')
def logout():
    session['logged_in'] = False
    if not session['logged_in']:
        return redirect(url_for('index'))


@app.route("/fetchMap")
def fetchMap():
    try:
        c, conn = connection()
        c.execute(
            "SELECT t.`Subject`, t.Lat, t.Lng,t.cn FROM t_report AS t WHERE t.`Status` = 'Pending' ORDER BY t.cn DESC")
        DataList = c.fetchall()
        return json.dumps(DataList, sort_keys=True, ensure_ascii=False)
    except Exception as e:
        return str(e)


@app.route("/getNewCount")
def getNewCount():
    try:
        c, conn = connection()
        c.execute("call sp_new_data()")
        d = c.fetchone()
        data = {
            'reportnew': d[0],
            'usernew': d[1],
            'anmtnew': d[2]
        }
    except KeyError:
        abort(404)
    return jsonify({'records': data})


@app.route('/fetchReport')
def fetchReport():
    try:
        c, conn = connection()
        c.execute("call sp_report()")
        data_list = c.fetchall()
        return json.dumps(data_list, default=str, sort_keys=True, ensure_ascii=False)
    except Exception as e:
        return str(e)


# get report by id
@app.route("/reportbyid/<int:cn>")
def reportbyid(cn):
    try:
        c, conn = connection()
        c.execute("call sp_report_bycn('%s')" % (str(cn)))
        d = c.fetchone()
        # session['lat'] = d[3]
        # session['lng'] = d[4]
        data = {
            'report_cn': d[0], 'Subject': d[1], 'Description': d[2], 'Lat': d[3],
            'Lng': d[4],
            'fileCode': d[5],
            'ContactNo': d[7],
            'Date': d[8],
            'Status': d[9],
            'fullname': d[10],
        }
    except KeyError:
        abort(404)
    return jsonify({'data': data})


@app.route('/report_info')
def report_info():
    return render_template('report_info.html')


@app.route('/getImages/<int:fileCode>')
def getImages(fileCode):
    c, conn = connection()
    c.execute("Select Filename from t_files WHERE FileCode = '%s'" % fileCode)
    dataList = c.fetchall()
    # return render_template('report_info.html', dataList=dataList)
    return json.dumps(dataList, default=str, sort_keys=True, ensure_ascii=False)
    # data = {'file':dataList[0],}
    # return jsonify({'data':dataList})


port = random.randint(1000, 6000)
if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True, host='localhost', port=7070)
